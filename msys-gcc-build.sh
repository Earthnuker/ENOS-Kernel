export TARGET=$1
export PREFIX="$HOME/opt/cross/$TARGET/"
export PATH="$PREFIX/bin:$PATH"

mkdir build-binutils
cd build-binutils
../binutils-*/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make -j8
make install
cd ..

which -- $TARGET-as || (echo $TARGET-as is not in the PATH && exit)

mkdir build-gcc
cd build-gcc
../gcc-*/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make -j8 all-gcc
make -j8 all-target-libgcc
make install-gcc
make install-target-libgcc
cd ..
