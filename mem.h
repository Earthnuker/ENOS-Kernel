#ifndef MEM_H
#define MEM_H
#include <stddef.h>
#include <stdint.h>

void* memcpy(void* restrict dst_ptr, const void* restrict src_ptr, size_t size){
  unsigned char* dst = (unsigned char*) dst_ptr;
  const unsigned char* src = (const unsigned char*) src_ptr;
  for (size_t i=0;i<size;++i) {
    dst[i]=src[i];
  }
  return dst_ptr;
}

void* memset(void* buf_ptr,int value,size_t size){
  unsigned char* buf = (unsigned char*) buf_ptr;
  for (size_t i = 0; i < size; ++i) {
    buf[i] = (unsigned char) value;
  }
  return buf_ptr;
}

void* memmove(void* dst_ptr, const void* src_ptr, size_t size)
{
	unsigned char* dst = (unsigned char*) dst_ptr;
	const unsigned char* src = (const unsigned char*) src_ptr;
	if ( dst < src )
		for ( size_t i = 0; i < size; i++ )
			dst[i] = src[i];
	else
		for ( size_t i = size; i != 0; i-- )
			dst[i-1] = src[i-1];
	return dst_ptr;
}


void* memmove_stride(void* dst_ptr, const void* src_ptr, size_t size,size_t stride)
{
	unsigned char* dst = (unsigned char*) dst_ptr;
	const unsigned char* src = (const unsigned char*) src_ptr;
	if ( dst < src )
		for ( size_t i = 0; i < size; i+=stride )
			dst[i] = src[i];
	else
		for ( size_t i = size; i > 0; i-=stride )
			dst[i-1] = src[i-1];
	return dst_ptr;
}

#endif
