#if !defined(__cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default. */
#endif
#define arrsize(a)  (sizeof(a)/sizeof(a[0]))
#define getrand(a) (a[rand()%arrsize(a)])
#include <stddef.h>
#include <stdint.h>
#include "tty.h"
/* Check if the compiler thinks we are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif

/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
#error "This tutorial needs to be compiled with a ix86-elf compiler"
#endif

#if defined(__cplusplus)
extern "C" /* Use C linkage for kmain. */
#endif

uint32_t x=12021993, y=1993, z=1202, w=1337;

uint32_t rand(void) {
    uint32_t t = x;
    t ^= t << 11;
    t ^= t >> 8;
    x = y; y = z; z = w;
    w ^= w >> 19;
    w ^= t;
    return w;
}
void print_banner() {
  terminal_setcolor(make_color(COLOR_RED, COLOR_BLACK));
  terminal_writestring("    _______   ______  _____\n\r   / ____/ | / / __ \\/ ___/\n\r  / __/ /  |/ / / / /\\__ \\ \n\r / /___/ /|  / /_/ /___/ / \n\r/_____/_/ |_/\\____//____/\n\r");
  terminal_setcolor(make_color(COLOR_LIGHT_GREY, COLOR_BLACK));
}
void kmain(unsigned int mb_magic) {
  /* Initialize terminal interface */
  terminal_initialize();
  if (mb_magic != 0x2BADB002){
    terminal_setcolor(make_color(COLOR_RED,COLOR_BLACK)|(1<<7));
    terminal_writestring("Invalid Multiboot Magic!\n\r");
    return;
  }
  uint32_t n=1;
  uint8_t fg,bg;
  uint8_t primary_cols[] = {COLOR_RED,COLOR_GREEN,COLOR_BLUE};
  uint8_t primary_cols_light[] = {COLOR_LIGHT_RED,COLOR_LIGHT_GREEN,COLOR_LIGHT_BLUE};
  uint8_t cols[] = {
                    COLOR_BLACK,
                    primary_cols[1],
                    primary_cols_light[1],
                    COLOR_WHITE,
                  };
  char digits[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  #define n_cols 24
  #define DELAY (1<<24)
  uint32_t pos[n_cols]={0};
  uint32_t col_len[n_cols]={0};
  for (int i=0;i<n_cols;i++){
    pos[i]=rand()%VGA_WIDTH;
    col_len[i]=5+(rand()%(VGA_HEIGHT>>1));
  }
  while (1){
    ++n;
    for (int i=0;i<n_cols;i++){
      fg = getrand(cols);
      bg = getrand(cols);
      while (fg==bg){
        fg = getrand(cols);
        bg = getrand(cols);
      }
      terminal_putchar(getrand(digits), make_color(fg,bg), pos[i], 0);
    }
    for (size_t it=0;it<VGA_WIDTH;it++){
      size_t rand_col;
      bool used=true;
      while (used) {
        rand_col=rand()%VGA_WIDTH;
        used=false;
        for (int i=0;i<n_cols;i++){
          if (rand_col==pos[i]) used=true;
        }
      };
      terminal_scroll_col_inv(rand_col);
    };
    for (int i=0;i<n_cols;i++){
      if ((n%(col_len[i]+1))==0){
        pos[i]=rand()%VGA_WIDTH;
        col_len[i]=5+(rand()%(VGA_HEIGHT>>1));
      }
    }
    for (uint32_t a=0;a<DELAY;a++){
      asm("nop");
    }
    terminal_scroll_inv();
  }
}
