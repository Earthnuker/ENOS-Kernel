#ifndef TTY_H
#define TTY_H
#include <stddef.h>
#include <stdint.h>
#include "mem.h"
#define VIDEO 0xB8000
/* Hardware text mode color constants. */
enum vga_color {
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_MAGENTA = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11,
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13,
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
};

uint8_t make_color(enum vga_color fg, enum vga_color bg) {
	return fg | bg << 4;
}

uint16_t make_vgaentry(char c, uint8_t color) {
	uint16_t c16 = c;
	uint16_t color16 = color;
	return c16 | color16 << 8;
}

size_t strlen(const char* str) {
	size_t ret = 0;
	while ( str[ret] )
		ret++;
	return ret;
}

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

void terminal_initialize() {
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
	terminal_buffer = (uint16_t*) VIDEO;
	for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = make_vgaentry(' ', terminal_color);
		}
	}
}


void terminal_scroll() {
	memmove(terminal_buffer,terminal_buffer+(VGA_WIDTH),VGA_WIDTH*VGA_HEIGHT*2);
	memset(terminal_buffer+(VGA_WIDTH*(VGA_HEIGHT-1)*2),0,VGA_WIDTH*2);
	return;
}

void terminal_scroll_inv() {
	memmove(terminal_buffer+(VGA_WIDTH),terminal_buffer,VGA_WIDTH*VGA_HEIGHT*2);
	memset(terminal_buffer,0,VGA_WIDTH*2);
	return;
}

void terminal_scroll_col_inv(size_t col) {
	size_t line;
	size_t from_offset;
	size_t to_offset;
	for (line=VGA_HEIGHT;line!=0;line--){
		from_offset=(col+VGA_WIDTH*(line-1));
		to_offset=(col+VGA_WIDTH*line);
		terminal_buffer[to_offset]=terminal_buffer[from_offset];
	}
	terminal_buffer[from_offset]=0;
}

void terminal_scroll_col(size_t col) {
	size_t line;
	size_t from_offset;
	size_t to_offset;
	for (line=1;line<VGA_HEIGHT;line++){
		from_offset=(col+VGA_WIDTH*line);
		to_offset=(col+VGA_WIDTH*(line-1));
		terminal_buffer[to_offset]=terminal_buffer[from_offset];
	}
	memset(terminal_buffer+col,0,2);
}

void terminal_setcolor(uint8_t color) {
	terminal_color = color;
}

void terminal_putchar(char c, uint8_t color, size_t x, size_t y) {
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = make_vgaentry(c, color);
}

void terminal_writechar(char c) {
  switch(c){
    case '\n':
	    if ((terminal_row+1) >= VGA_HEIGHT) {
				terminal_scroll();
	    } else {
				++terminal_row;
			}
      return;
    case '\r':
      terminal_column=0;
      return;
		case '\b':
			if (terminal_column) {
				terminal_column--;
			}
			return;
  }
	terminal_putchar(c, terminal_color, terminal_column, terminal_row);
  if ((++terminal_column) >= VGA_WIDTH) {
    terminal_column = 0;
    if ((++terminal_row) >= VGA_HEIGHT) {
      terminal_column = 0;
			terminal_row=(VGA_HEIGHT-1);
      terminal_scroll();
    }
  }
}

void terminal_writestring(const char* data) {
	size_t datalen = strlen(data);
	for (size_t i = 0; i < datalen; i++) {
		terminal_writechar(data[i]);
  }
}
#endif
