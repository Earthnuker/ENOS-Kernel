You need a i686-elf cross compiler to compile this kernel

**These Instuctions are for Windows (only tested on Windows 7 64-bit)**

To build it on windows using MSYS2:

- Install [MSYS2](https://msys2.github.io/) (follow the instructions on the page)

- install the required build dependencies with
`pacman -S base base-devel gcc make bison flex texinfo {gmp,mpfr,mpc}-devel`

- download [binutils-2.26](ftp://sourceware.org/pub/binutils/releases/) and [gcc-5.4.0](ftp://ftp.gnu.org/gnu/gcc/) and extract both to the same folder so it looks like this:
```
folder-name/gcc-5.4.0/ABOUT-NLS
folder-name/gcc-5.4.0/ChangeLog
folder-name/gcc-5.4.0/ChangeLog.jit
...
folder-name/binutils-2.26/ChangeLog
folder-name/binutils-2.26/compile
folder-name/binutils-2.26/configure
...
```
- copy `msys-gcc-build.sh` into the folder where both gcc and binutils are (*folder-name* in this example)

- run `sh msys-gcc-build.sh i686-elf` from *folder-name*, 
this will build the cross-compiler and place it in `$HOME/opt/cross/i686-elf/` 
(you should add that to your $PATH even though the Makefile does it for you)

- to compile the kernel run `make clean all`

- to boot it in [QEMU](http://qemu.weilnetz.de/) run `qemu-system-i386 -kernel kernel.bin`

- if you want to boot it on a real system you can make a bootable syslinux drive using Rufus (on windows)

- you will nedd libcom32.c32 and mboot.c32 from [syslinux](https://www.kernel.org/pub/linux/utils/boot/syslinux/6.xx/)
(found in  `bios/com32/mboot/` and `bios/com32/lib/` in the syslinux zip))

- format the drive using [Rufus](http://rufus.akeo.ie/) (***THIS WILL DELETE ALL DATA ON THE DRIVE!***)
(check "create bootable disk using" and select "Syslinux 6.03" from the dropdown)

- finally copy libcom32.c32, mboot.c32, the syslinux.cfg from this repository and the kernel.bin you just compiled onto the newly formatted usb drive and boot from it

Thaks to [Alek](https://github.com/alekratz) for writing most of the Makefile
