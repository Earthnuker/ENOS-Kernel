SOURCES_C=kernel.c
SOURCES_A=boot.s
OBJDIR=obj
OBJS=$(SOURCES_C:%.c=$(OBJDIR)/%.o) $(SOURCES_A:%.s=$(OBJDIR)/%.o)
TARGET=i686-elf
BINFILE=kernel.bin
AS=$(TARGET)-as
CC=$(TARGET)-gcc
CFLAGS=-c -std=gnu99 -O2 -Wall -Wextra -ffreestanding -s
LD=$(TARGET)-gcc
LDFLAGS=-nostdlib -lgcc -Wall -Wextra -s
FLOPPY=/a/
export PATH := $(HOME)/opt/cross/$(TARGET)/bin/:$(PATH)


.PHONY: all
all: $(BINFILE)

$(BINFILE): $(OBJS)
	$(LD) $(LDFLAGS) -T linker.ld -o $(BINFILE) $(OBJS)


$(OBJDIR)/%.o: %.c | $(OBJDIR)
	$(CC) $(CFLAGS) $< -o $@

$(OBJDIR)/%.o: %.s | $(OBJDIR)
	$(AS) $< -o $@

$(OBJDIR):
	mkdir -pv $(OBJDIR)

.PHONY: clean
clean:
	rm -f $(OBJS)

.PHONY: install
install:
	cp -v $(BINFILE) $(FLOPPY)
